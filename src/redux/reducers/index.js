import characters from './characters';
import episodes from './episodes';
import locations from './locations';

export default {
  characters,
  episodes,
  locations,
};
