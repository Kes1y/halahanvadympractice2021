import { createReducer } from '@reduxjs/toolkit';
import {
  getLocations,
  getLocationsByPage,
  getLocationsByFilter,
} from '../../redux/actions/locations';

const initialState = {
  data: [],
};

export default createReducer(initialState, {
  [getLocations.fulfilled]: (state, { payload }) => {
    return {
      ...state,
      data: payload,
    };
  },
  [getLocationsByPage.fulfilled]: (state, { payload }) => {
    return {
      ...state,
      data: {
        info: payload?.info,
        results: [...state.data?.results, ...payload.results],
      },
    };
  },
  [getLocationsByFilter.fulfilled]: (state, { payload }) => {
    return {
      ...state,
      data: payload,
    };
  },
});
