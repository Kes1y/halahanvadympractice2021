import { createReducer } from '@reduxjs/toolkit';
import {
  getCharacters,
  getCharacterEpisodeById,
  getCharactersByPage,
  getCharactersByFilter,
  getCharactersByFilterAndByPage,
} from '../../redux/actions/characters';

const initialState = {
  data: [],
};

export default createReducer(initialState, {
  [getCharacters.fulfilled]: (state, { payload }) => {
    return {
      ...state,
      data: payload,
    };
  },
  [getCharactersByFilter.fulfilled]: (state, { payload }) => {
    return {
      ...state,
      data: payload,
    };
  },
  [getCharactersByPage.fulfilled]: (state, { payload }) => {
    return {
      ...state,
      data: payload,
    };
  },
  [getCharactersByFilterAndByPage.fulfilled]: (state, { payload }) => {
    return {
      ...state,
      data: payload,
    };
  },
  [getCharacterEpisodeById.fulfilled]: (state, { payload }) => {
    return {
      ...state,
      episodes: {
        ...state.episodes,
        [payload?.id]: payload,
      },
    };
  },
});
