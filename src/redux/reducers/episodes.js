import { createReducer } from '@reduxjs/toolkit';
import {
  getEpisodes,
  getEpisodesByPage,
  getEpisodesByName,
} from '../../redux/actions/episodes';

const initialState = {
  data: [],
};

export default createReducer(initialState, {
  [getEpisodes.fulfilled]: (state, { payload }) => {
    return {
      ...state,
      data: payload,
    };
  },
  [getEpisodesByPage.fulfilled]: (state, { payload }) => {
    return {
      ...state,
      data: {
        info: payload?.info,
        results: [...state.data?.results, ...payload.results],
      },
    };
  },
  [getEpisodesByName.fulfilled]: (state, { payload }) => {
    return {
      ...state,
      data: payload,
    };
  },
});
