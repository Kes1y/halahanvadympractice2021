import { createAsyncThunk } from '@reduxjs/toolkit';
import api from '../../utils/api';

export const getCharacters = createAsyncThunk('getCharacters', async () => {
  try {
    const res = await api.get(`/character/`);

    return res.data;
  } catch (e) {
    return null;
  }
});

export const getCharactersByPage = createAsyncThunk(
  'getCharactersByPage',
  async (page) => {
    try {
      const res = await api.get(`/character/?page=${page}`);

      return res.data;
    } catch (e) {
      return null;
    }
  },
);

export const getCharactersByFilter = createAsyncThunk(
  'getCharactersByFilter',
  async (filteredValue) => {
    try {
      const keys = Object.keys(filteredValue);
      const values = Object.values(filteredValue);
      let res;
      if (keys.length === 1) {
        res = await api.get(`/character/?${keys[0]}=${values[0]}`);
      } else if (keys.length === 2) {
        res = await api.get(
          `/character/?${keys[0]}=${values[0]}&${keys[1]}=${values[1]}`,
        );
      } else if (keys.length === 3) {
        res = await api.get(
          `/character/?${keys[0]}=${values[0]}&${keys[1]}=${values[1]}&${keys[2]}=${values[2]}`,
        );
      }

      return res.data;
    } catch (e) {
      return null;
    }
  },
);

export const getCharactersByFilterAndByPage = createAsyncThunk(
  'getCharactersByFilterAndByPage',
  async ({ filteredValue, page }) => {
    try {
      const keys = Object.keys(filteredValue);
      const values = Object.values(filteredValue);
      let res;
      if (keys.length === 1) {
        res = await api.get(`/character/?page=${page}&${keys[0]}=${values[0]}`);
      } else if (keys.length === 2) {
        res = await api.get(
          `/character/?page=${page}&${keys[0]}=${values[0]}&${keys[1]}=${values[1]}`,
        );
      } else if (keys.length === 3) {
        res = await api.get(
          `/character/?page=${page}&${keys[0]}=${values[0]}&${keys[1]}=${values[1]}&${keys[2]}=${values[2]}`,
        );
      }

      return res.data;
    } catch (e) {
      return null;
    }
  },
);

export const getCharacterEpisodeById = createAsyncThunk(
  'getCharacterEpisodeById',
  async (id) => {
    try {
      const res = await api.get(`/episode/${id}`);

      return res.data;
    } catch (e) {
      return null;
    }
  },
);
