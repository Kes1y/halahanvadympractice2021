import { createAsyncThunk } from '@reduxjs/toolkit';
import api from '../../utils/api';

export const getLocations = createAsyncThunk('getLocations', async () => {
  try {
    const res = await api.get(`/location`);

    return res.data;
  } catch (e) {
    return null;
  }
});

export const getLocationsByPage = createAsyncThunk(
  'getLocationsByPage',
  async (page) => {
    try {
      const res = await api.get(`/location/?page=${page}`);

      return res.data;
    } catch (e) {
      return null;
    }
  },
);

export const getLocationsByFilter = createAsyncThunk(
  'getLocationsByFilter',
  async (filteredValue) => {
    try {
      const keys = Object.keys(filteredValue);
      const values = Object.values(filteredValue);
      let res;
      if (keys.length === 1) {
        res = await api.get(`/location/?${keys[0]}=${values[0]}`);
      } else if (keys.length === 2) {
        res = await api.get(
          `/location/?${keys[0]}=${values[0]}&${keys[1]}=${values[1]}`,
        );
      } else if (keys.length === 3) {
        res = await api.get(
          `/location/?${keys[0]}=${values[0]}&${keys[1]}=${values[1]}&${keys[2]}=${values[2]}`,
        );
      }

      return res.data;
    } catch (e) {
      return null;
    }
  },
);
