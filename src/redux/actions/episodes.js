import { createAsyncThunk } from '@reduxjs/toolkit';
import api from '../../utils/api';

export const getEpisodes = createAsyncThunk('getEpisodes', async () => {
  try {
    const res = await api.get(`/episode`);

    return res.data;
  } catch (e) {
    return null;
  }
});

export const getEpisodesByPage = createAsyncThunk(
  'getEpisodesByPage',
  async (page) => {
    try {
      const res = await api.get(`/episode/?page=${page}`);

      return res.data;
    } catch (e) {
      return null;
    }
  },
);

export const getEpisodesByName = createAsyncThunk(
  'getEpisodesByName',
  async (filteredName) => {
    try {
      const res = await api.get(`/episode/?name=${filteredName}`);

      return res.data;
    } catch (e) {
      return null;
    }
  },
);
