export const charactersSelector = (state) => {
  return state?.characters.data;
};

export const infoSelector = (state) => {
  return state?.characters.info;
};

export const characterEpisodesSelector = (state) => {
  return state?.characters.episodes;
};
