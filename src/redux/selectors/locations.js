// eslint-disable-next-line import/prefer-default-export
export const locationsSelector = (state) => {
  return state?.locations.data;
};
