// eslint-disable-next-line import/prefer-default-export
export const episodesSelector = (state) => {
  return state?.episodes.data;
};
