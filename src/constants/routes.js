export const MAIN = '/';
export const CHARACTERS = '/characters';
export const EPISODES = '/episodes';
export const LOCATIONS = '/locations';
export const MY_WATCH_LIST = '/my-watch-list';
