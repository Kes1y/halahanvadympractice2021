import axios from 'axios';

const api = axios.create();
api.defaults.baseURL = 'https://rickandmortyapi.com/api';

export default api;
