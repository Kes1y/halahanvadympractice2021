// eslint-disable-next-line import/prefer-default-export
export const getRows = (data, columns) => {
  return data?.map((item) => {
    return {
      [columns?.[0]?.id]: item[columns?.[0]?.id],
      [columns?.[1]?.id]: item[columns?.[1]?.id],
      [columns?.[2]?.id]: item[columns?.[2]?.id],
    };
  });
};
