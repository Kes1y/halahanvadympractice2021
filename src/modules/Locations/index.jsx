import React from 'react';
import { getRows } from '../../utils';
import { Layout, SearchInput, Table } from '../../components';
import { useLocations } from '../../hooks';
import st from './styles.module.css';

export default function Locations() {
  const {
    handleNameChange,
    handleTypeChange,
    handleDimensionChange,
    columns,
    locations,
  } = useLocations();

  return (
    <Layout>
      <div className={st.filter__block}>
        <SearchInput
          id="standard-basic"
          label="Name"
          onChange={handleNameChange}
        />
        <SearchInput
          id="standard-basic"
          label="Type"
          onChange={handleTypeChange}
        />
        <SearchInput
          id="standard-basic"
          label="Dimension"
          onChange={handleDimensionChange}
        />
      </div>
      <Table
        columns={columns}
        rows={getRows(locations?.results, columns)}
        rowsPerPage={25}
      />
    </Layout>
  );
}
