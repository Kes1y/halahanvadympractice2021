import React from 'react';
import { getRows } from '../../utils';
import { Layout, SearchInput, Table } from '../../components';
import { useEpisodes } from '../../hooks';
import st from './styles.module.css';

export default function Episodes() {
  const { handleNameChange, columns, episodes } = useEpisodes();

  return (
    <Layout>
      <div className={st.filter__block}>
        <SearchInput
          id="standard-basic"
          label="Name"
          onChange={handleNameChange}
        />
      </div>
      <Table
        columns={columns}
        rows={getRows(episodes.results, columns)}
        rowsPerPage={25}
      />
    </Layout>
  );
}
