import React from 'react';
import { Switch, Route } from 'react-router-dom';
import {
  MAIN,
  CHARACTERS,
  EPISODES,
  LOCATIONS,
  MY_WATCH_LIST,
} from '../constants/routes';
import Characters from './Characters';
import Episodes from './Episodes';
import Locations from './Locations/index';
import MyWatchList from './MyWatchList';

export default function App() {
  return (
    <Switch>
      <Route exact path={MAIN} component={Characters} />
      <Route path={CHARACTERS} component={Characters} />
      <Route path={EPISODES} component={Episodes} />
      <Route path={LOCATIONS} component={Locations} />
      <Route path={MY_WATCH_LIST} component={MyWatchList} />
    </Switch>
  );
}
