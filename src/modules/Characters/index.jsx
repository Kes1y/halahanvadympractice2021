import React from 'react';
import { Layout, Button, RadioButtons, SearchInput } from '../../components';
import { NavigateNext, NavigateBefore } from '@material-ui/icons';
import { useCharacters } from '../../hooks';
import { Grid } from './components';
import st from './styles.module.css';

export default function Characters() {
  const {
    handleGenderChange,
    handleStatusChange,
    handleSpeciesChange,
    characters,
    idRange,
    handleClickBefore,
    handleClickNext,
  } = useCharacters();

  return (
    <Layout>
      <div className={st.filter__block}>
        <RadioButtons
          handleChange={handleGenderChange}
          name="Gender"
          data={[
            { value: 'male', label: 'Male' },
            { value: 'female', label: 'Female' },
            { value: 'genderless', label: 'Genderless' },
            { value: 'unknown', label: 'Unknown' },
          ]}
        />
        <RadioButtons
          handleChange={handleStatusChange}
          name="Status"
          data={[
            { value: 'alive', label: 'Alive' },
            { value: 'dead', label: 'Dead' },
            { value: 'unknown', label: 'Unknown' },
          ]}
        />
        <SearchInput
          id="standard-basic"
          label="Species"
          onChange={handleSpeciesChange}
        />
      </div>
      <Grid
        data={characters?.results?.filter((item, index) => {
          if (index >= idRange.start && index <= idRange.end) {
            return item;
          }
          return null;
        })}
      />
      <div className={st.pagination__block}>
        <Button
          onClick={handleClickBefore}
          icon={<NavigateBefore />}
          disabled={!characters?.info?.prev && idRange.start === 0}
        />
        <Button
          onClick={handleClickNext}
          icon={<NavigateNext />}
          disabled={
            idRange.end >= characters?.results?.length - 1 &&
            !characters?.info?.next
          }
        />
      </div>
    </Layout>
  );
}
