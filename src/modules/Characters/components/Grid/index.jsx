/* eslint-disable react/forbid-prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import { Grid } from '@material-ui/core';
import { Card } from '../../../../components';

export default function CustomGrid({ data }) {
  return (
    <Grid container>
      <Grid item>
        <Grid container justify="center">
          {data?.map(
            (
              {
                image,
                name,
                location,
                episode,
                gender,
                origin,
                species,
                status,
              },
              index,
            ) => {
              return (
                <Card
                  // eslint-disable-next-line react/no-array-index-key
                  key={index}
                  image={image}
                  name={name}
                  cardContent={[
                    { label: 'Last known location', text: location.name },
                    { label: 'First seen in', text: episode[0], isLink: true },
                  ]}
                  popoverContent={[
                    { label: 'Gender', text: gender },
                    { label: 'Origin', text: origin.name },
                    { label: 'Species', text: species },
                    { label: 'Status', text: status },
                  ]}
                />
              );
            },
          )}
        </Grid>
      </Grid>
    </Grid>
  );
}

CustomGrid.defaultProps = {
  data: [],
};

CustomGrid.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      image: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      location: PropTypes.object,
      episode: PropTypes.array,
      gender: PropTypes.string.isRequired,
      origin: PropTypes.string.isRequired,
      species: PropTypes.string.isRequired,
      status: PropTypes.string.isRequired,
    }),
  ),
};
