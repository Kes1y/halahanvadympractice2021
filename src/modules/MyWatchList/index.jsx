import React from 'react';
import { Layout, Table, Input, Button } from '../../components';
import { useMyWatchList } from '../../hooks';
import st from './styles.module.css';

export default function MyWatchList() {
  const {
    handleSubmit,
    handleChange,
    columns,
    episodes,
    getRows,
  } = useMyWatchList();

  return (
    <Layout>
      <form onSubmit={handleSubmit} className={st.form}>
        <Input
          id="standard-basic"
          onChange={handleChange}
          label="Write episode"
        />
        <Button text="Add new episode" type="submit" />
      </form>
      <Table columns={columns} rowsPerPage={10} rows={getRows(episodes)} />
    </Layout>
  );
}
