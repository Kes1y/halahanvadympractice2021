/* eslint-disable react/jsx-filename-extension */
import React, { useEffect, useState } from 'react';
import { Button, CheckBox } from '../components';

const columns = [
  { id: 'name', label: 'Name' },
  { id: 'isViewed', label: 'Viewed' },
  { id: 'options', label: 'Options' },
];

export default function useEpisodes() {
  const [inputValue, setInputValue] = useState(null);
  const [episodes, setEpisodes] = useState([]);

  const getDataFromLocaleStorage = () => {
    const tmp = [];
    for (let i = 0; i < window.localStorage.length; i += 1) {
      const key = window.localStorage.key(i);
      tmp.push({
        episode: key,
        isViewed: window.localStorage.getItem(key),
      });
    }
    setEpisodes(tmp);
  };

  useEffect(() => {
    getDataFromLocaleStorage();
  }, []);

  const getRows = (data) => {
    const handleDeleteClick = (id) => {
      window.localStorage.removeItem(id);
      getDataFromLocaleStorage();
    };

    const handleCheckBoxChange = (id) => {
      const value = +window.localStorage.getItem(id);
      window.localStorage.setItem(id, value ? 0 : 1);
      getDataFromLocaleStorage();
    };

    return data.map(({ episode, isViewed }) => {
      return {
        name: episode,
        isViewed: (
          <CheckBox
            ariaLabel="primary checkbox"
            checked={!!+isViewed}
            onChange={() => handleCheckBoxChange(episode)}
          />
        ),
        options: (
          <Button
            text="Delete"
            color="secondary"
            variant="contained"
            onClick={() => handleDeleteClick(episode)}
          />
        ),
      };
    });
  };

  const handleSubmit = () => {
    if (inputValue) {
      window.localStorage.setItem(inputValue, 0);
      getDataFromLocaleStorage();
    }
  };

  const handleChange = (e) => {
    setInputValue(e.target.value);
  };

  return {
    handleSubmit,
    handleChange,
    columns,
    episodes,
    getRows,
  };
}
