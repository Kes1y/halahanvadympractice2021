import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  getLocations,
  getLocationsByPage,
  getLocationsByFilter,
} from '../redux/actions/locations';
import { locationsSelector } from '../redux/selectors/locations';

const columns = [
  { id: 'name', label: 'Name' },
  { id: 'type', label: 'Type' },
  { id: 'dimension', label: 'Dimension' },
];

export default function useEpisodes() {
  const dispatch = useDispatch();
  const locations = useSelector(locationsSelector);
  const [filteredValue, setFilteredValue] = useState(null);

  useEffect(() => {
    dispatch(getLocations());
  }, [dispatch]);

  useEffect(() => {
    if (locations?.info?.next) {
      dispatch(
        getLocationsByPage(
          locations.info?.next.substr(
            locations.info.next.lastIndexOf('page') + 5,
          ),
        ),
      );
    }
  }, [dispatch, locations]);

  useEffect(() => {
    if (filteredValue) {
      dispatch(getLocationsByFilter(filteredValue));
    }
  }, [dispatch, filteredValue]);

  const handleNameChange = (name) => {
    setFilteredValue({ ...filteredValue, name });
  };

  const handleTypeChange = (type) => {
    setFilteredValue({ ...filteredValue, type });
  };

  const handleDimensionChange = (dimension) => {
    setFilteredValue({ ...filteredValue, dimension });
  };

  return {
    handleNameChange,
    handleTypeChange,
    handleDimensionChange,
    columns,
    locations,
  };
}
