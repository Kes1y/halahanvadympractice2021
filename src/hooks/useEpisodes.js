import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  getEpisodes,
  getEpisodesByPage,
  getEpisodesByName,
} from '../redux/actions/episodes';
import { episodesSelector } from '../redux/selectors/episodes';

const columns = [
  { id: 'name', label: 'Name' },
  { id: 'air_date', label: 'Show date' },
  { id: 'episode', label: 'Episode' },
];

export default function useEpisodes() {
  const dispatch = useDispatch();
  const episodes = useSelector(episodesSelector);

  useEffect(() => {
    dispatch(getEpisodes());
  }, [dispatch]);

  useEffect(() => {
    if (episodes.info?.next) {
      dispatch(
        getEpisodesByPage(
          episodes.info?.next.substr(
            episodes.info.next.lastIndexOf('page') + 5,
          ),
        ),
      );
    }
  }, [dispatch, episodes]);

  const handleNameChange = (name) => {
    dispatch(getEpisodesByName(name));
  };

  return {
    handleNameChange,
    columns,
    episodes,
  };
}
