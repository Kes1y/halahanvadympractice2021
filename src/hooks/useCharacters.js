import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { createSelector } from 'reselect';
import {
  getCharacters,
  getCharactersByPage,
  getCharactersByFilter,
  getCharactersByFilterAndByPage,
} from '../redux/actions/characters';
import { charactersSelector, infoSelector } from '../redux/selectors/characters';

const selector = createSelector(
  charactersSelector,
  infoSelector,
  (characters, info) => ({
    characters,
    info,
  }),
);

export default function useCharacters() {
  const dispatch = useDispatch();
  const { characters } = useSelector(selector);
  const [idRange, setIdRange] = useState({ start: 0, end: 9 });
  const [filteredValue, setFilteredValue] = useState(null);

  useEffect(() => {
    dispatch(getCharacters());
  }, [dispatch]);

  const handleClickBefore = () => {
    if (idRange.start !== 0 && idRange.end === 19) {
      setIdRange({
        start: 0,
        end: 9,
      });
    } else {
      setIdRange({
        start: 10,
        end: 19,
      });
      if (filteredValue) {
        return dispatch(
          getCharactersByFilterAndByPage({
            filteredValue,
            page: characters.info.prev.substr(
              characters.info.prev.lastIndexOf('page') + 5,
              1,
            ),
          }),
        );
      }
      dispatch(
        getCharactersByPage(
          characters.info?.prev.substr(
            characters.info.prev.lastIndexOf('e') + 2,
          ),
        ),
      );
    }
    return null;
  };

  const handleClickNext = () => {
    if (idRange.start === 0 && idRange.end !== 19) {
      setIdRange({
        start: 10,
        end: 19,
      });
    } else {
      setIdRange({
        start: 0,
        end: 9,
      });
      if (filteredValue) {
        return dispatch(
          getCharactersByFilterAndByPage({
            filteredValue,
            page: characters.info?.next.substr(
              characters.info.next.lastIndexOf('page') + 5,
              1,
            ),
          }),
        );
      }
      dispatch(
        getCharactersByPage(
          characters.info?.next.substr(
            characters.info.next.lastIndexOf('e') + 2,
          ),
        ),
      );
    }
    return null;
  };

  useEffect(() => {
    setIdRange({
      start: 0,
      end: 9,
    });
    dispatch(getCharactersByFilter(filteredValue));
  }, [dispatch, filteredValue]);

  const handleGenderChange = (e) => {
    setFilteredValue({ ...filteredValue, gender: e.target.value });
  };

  const handleStatusChange = (e) => {
    setFilteredValue({ ...filteredValue, status: e.target.value });
  };

  const handleSpeciesChange = (species) => {
    setFilteredValue({ ...filteredValue, species });
  };

  return {
    handleGenderChange,
    handleStatusChange,
    handleSpeciesChange,
    characters,
    idRange,
    handleClickBefore,
    handleClickNext,
  };
}
