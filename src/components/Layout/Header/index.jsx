import React from 'react';
import { Link } from 'react-router-dom';
import { AppBar, Toolbar } from '@material-ui/core';
import {
  CHARACTERS,
  EPISODES,
  LOCATIONS,
  MY_WATCH_LIST,
} from '../../../constants/routes';
import st from './styles.module.css';

export default function Header() {
  return (
    <AppBar position="static">
      <Toolbar className={st.header}>
        <Link to={CHARACTERS} style={{}}>
          Characters
        </Link>
        <Link to={EPISODES} style={{}}>
          Episodes
        </Link>
        <Link to={LOCATIONS} style={{}}>
          Locations
        </Link>
        <Link to={MY_WATCH_LIST} style={{}}>
          My watch list
        </Link>
      </Toolbar>
    </AppBar>
  );
}
