import React from 'react';
import { BottomNavigation, BottomNavigationAction } from '@material-ui/core';
import st from './styles.module.css';

export default function Footer() {
  return (
    <BottomNavigation className={st.footer} position="static" showLabels>
      <BottomNavigationAction label="©VadymHalahan" />
    </BottomNavigation>
  );
}
