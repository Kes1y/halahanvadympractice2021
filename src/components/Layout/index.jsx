import React from 'react';
import PropTypes from 'prop-types';
import Header from './Header';
import Footer from './Footer';
import st from './styles.module.css';

export default function Layout({ children }) {
  return (
    <>
      <Header />
      <div className={st.content}>{children}</div>
      <Footer />
    </>
  );
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};
