import React from 'react';
import PropTypes from 'prop-types';
import { Checkbox } from '@material-ui/core';

export default function CustomCheckbox({ checked, ariaLabel, onChange }) {
  return (
    <Checkbox
      checked={checked}
      inputProps={{ 'aria-label': ariaLabel }}
      onChange={onChange}
    />
  );
}

CustomCheckbox.defaultProps = {
  ariaLabel: 'primary checkbox',
};

CustomCheckbox.propTypes = {
  checked: PropTypes.bool.isRequired,
  ariaLabel: PropTypes.string,
  onChange: PropTypes.func.isRequired,
};
