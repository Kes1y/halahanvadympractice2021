/* eslint-disable array-callback-return */
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { getCharacterEpisodeById } from '../../redux/actions/characters';
import { characterEpisodesSelector } from '../../redux/selectors/characters';
import {
  Card,
  CardActionArea,
  CardMedia,
  Typography,
  CardContent,
} from '@material-ui/core';
import Popover from '../Popover';
import st from './styles.module.css';

export default function CustomCard(props) {
  const { image, name, cardContent, popoverContent } = props;
  const dispatch = useDispatch();
  const episode = useSelector(characterEpisodesSelector);
  const [episodeId, setEpisodeId] = useState();
  const [isOpen, setIsOpen] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);

  useEffect(() => {
    dispatch(getCharacterEpisodeById(episodeId));
  }, [dispatch, episodeId]);

  useEffect(() => {
    // eslint-disable-next-line no-unused-expressions
    cardContent?.map(({ text, isLink }) => {
      if (isLink) {
        setEpisodeId(text.substr(text.lastIndexOf('/') + 1));
      }
    });
  }, [cardContent]);

  const handleClick = (e) => {
    setAnchorEl(e.currentTarget);
    setIsOpen(true);
  };

  const handleClose = () => {
    setIsOpen(false);
  };

  return (
    <>
      <Card className={st.card} onClick={handleClick}>
        <CardActionArea>
          <CardMedia
            component="img"
            alt="Contemplative Reptile"
            image={image}
            title="Contemplative Reptile"
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              {name}
            </Typography>
            {cardContent?.map(({ label, text, isLink }) => {
              if (isLink) {
                return (
                  <Typography
                    key={text}
                    gutterBottom
                    variant="body1"
                    color="textSecondary"
                    component="p"
                  >
                    {`${label}: ${episode?.[episodeId]?.name}`}
                  </Typography>
                );
              }
              return (
                <Typography
                  key={text}
                  gutterBottom
                  variant="body1"
                  color="textSecondary"
                  component="p"
                >
                  {`${label}: ${text}`}
                </Typography>
              );
            })}
          </CardContent>
        </CardActionArea>
      </Card>
      <Popover isOpen={isOpen} handleClose={handleClose} anchorEl={anchorEl}>
        {popoverContent?.map(({ label, text }) => {
          return (
            <Typography
              key={text}
              gutterBottom
              variant="body1"
              color="textSecondary"
              component="p"
            >
              {`${label}: ${text}`}
            </Typography>
          );
        })}
      </Popover>
    </>
  );
}

CustomCard.defaultProps = {
  cardContent: [],
  popoverContent: [],
};

CustomCard.propTypes = {
  image: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  cardContent: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      text: PropTypes.string.isRequired,
      isLink: PropTypes.bool,
    }),
  ),
  popoverContent: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      text: PropTypes.string.isRequired,
    }),
  ),
};
