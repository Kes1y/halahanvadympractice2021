import React from 'react';
import PropTypes from 'prop-types';
import { Button } from '@material-ui/core';

export default function CustomButton({
  text,
  icon,
  onClick,
  disabled,
  type,
  color,
  variant,
}) {
  return (
    <Button
      onClick={onClick}
      disabled={disabled}
      type={type}
      color={color}
      variant={variant}
    >
      {icon || text}
    </Button>
  );
}

CustomButton.defaultProps = {
  text: null,
  icon: null,
  disabled: false,
  type: '',
  color: 'default',
  variant: 'text',
  onClick: null,
};

CustomButton.propTypes = {
  text: PropTypes.string,
  icon: PropTypes.element,
  onClick: PropTypes.func,
  disabled: PropTypes.bool,
  type: PropTypes.string,
  color: PropTypes.string,
  variant: PropTypes.string,
};
