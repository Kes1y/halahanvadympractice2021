import React from 'react';
import PropTypes from 'prop-types';
import { Popover } from '@material-ui/core';
import st from './styles.module.css';

export default function CustomPopover(props) {
  const { isOpen, handleClose, anchorEl, children } = props;

  return (
    <Popover
      open={isOpen}
      anchorEl={anchorEl}
      onClose={handleClose}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'center',
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'center',
      }}
    >
      <div className={st.content__wrapper}>{children}</div>
    </Popover>
  );
}

CustomPopover.defaultProps = {
  anchorEl: {},
};

CustomPopover.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  anchorEl: PropTypes.object,
  children: PropTypes.node.isRequired,
};
