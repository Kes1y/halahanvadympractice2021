import React from 'react';
import PropTypes from 'prop-types';
import { TextField } from '@material-ui/core';
import _delay from 'lodash/delay';
import st from './styles.module.css';

export default function SearchInput({ id, label, onChange }) {
  return (
    <div className={st.search__input}>
      <TextField
        id={id}
        label={label}
        onChange={(e) => _delay(onChange, 500, e.target.value)}
      />
    </div>
  );
}

SearchInput.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};
